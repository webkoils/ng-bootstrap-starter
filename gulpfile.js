var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-ruby-sass');
var watch = require('gulp-watch');
var browsersync = require('browser-sync');
var templateCache = require('gulp-angular-templatecache');


gulp.task('copy_templates',function(){
	gulp.src(['ts/**/*.html']).pipe(gulp.dest('app'));
});

gulp.task('copy_fonts',function(){
	
	gulp.src(['node_modules/font-awesome/fonts/*']).pipe(gulp.dest('assets/fonts'));
});
gulp.task('copy', ['copy_fonts','copy_templates']);

gulp.task('sass',function(){
	return sass('scss/app.scss',{
            loadPath: [ 'node_modules/font-awesome/scss', 'node_modules/bootstrap/scss', 'node_modules/motion-ui/src' ]
        })
        .on('error', sass.logError)
        .pipe(gulp.dest('css'));
});

gulp.task('watch',function(){
   	  gulp.watch('scss/**/*.scss', ['sass', browsersync.reload]);
    gulp.watch('ts/**/*.html', ['copy_templates', browsersync.reload]);
	   
	   
});

gulp.task('default',['copy','sass']);
